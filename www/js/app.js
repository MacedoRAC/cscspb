// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('app', {
            url: "/app",
            abstract: true,
            templateUrl: "right_menu.html"
        })

        .state('app.news', {
            url: "/news",
            views: {
                'menuContent' :{
                    templateUrl: "news.html"
                }
            }
        })

        .state('app.newsDescription', {
            url: "/news/:id",
            views: {
                'menuContent': {
                    templateUrl: "newsDescription.html"
                }
            }
        })

        .state('app.gallery', {
            url: "/gallery",
            views: {
                'menuContent' :{
                    templateUrl: "gallery.html"
                }
            }
        })

        .state('app.contacts', {
            url: "/contacts",
            views: {
                'menuContent' :{
                    templateUrl: "contacts.html",
                }
            }
        })

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/news');
})

.controller('GalleryCtrl', function ($scope, $ionicBackdrop, $ionicModal, $ionicSlideBoxDelegate, $ionicScrollDelegate) {

    $scope.allImages = [{
        src: 'img/upload/pic1.jpg'
    }, {
        src: 'img/upload/pic2.jpg'
    }, {
        src: 'img/upload/pic3.jpg'
    }];

    $scope.zoomMin = 1;


    $scope.showImages = function (index) {
        $scope.activeSlide = index;
        $scope.showModal('gallery-zoomview.html');
    };

    $scope.showModal = function (templateUrl) {
        $ionicModal.fromTemplateUrl(templateUrl, {
            scope: $scope
        }).then(function (modal) {
            $scope.modal = modal;
            $scope.modal.show();
        });
    }

    $scope.closeModal = function () {
        $scope.modal.hide();
        $scope.modal.remove()
    };

    $scope.updateSlideStatus = function (slide) {
        var zoomFactor = $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).getScrollPosition().zoom;
        if (zoomFactor == $scope.zoomMin) {
            $ionicSlideBoxDelegate.enableSlide(true);
        } else {
            $ionicSlideBoxDelegate.enableSlide(false);
        }
    };

});